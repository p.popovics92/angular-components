import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CoursesService } from '@app/services/courses.service';
import { Course } from '@app/shared/interfaces/course';
import { CoursesFacade } from '@app/store/courses/courses.facade';
import { Observable, combineLatest, of, switchMap } from 'rxjs';

@Component({
  selector: 'app-course-info',
  templateUrl: './course-info.component.html',
  styleUrls: ['./course-info.component.scss']
})
export class CourseInfoComponent implements OnInit{
  courseData$!: Observable<Course | null>;
  @Output() closeInfo: EventEmitter<boolean> = new EventEmitter();


  constructor(private router: Router, private coursesFacade: CoursesFacade, private coursesService: CoursesService) {}

  ngOnInit(): void {
    const url = this.router.url;
    const parts = url.split('/');
    const courseIdIndex = parts.indexOf('course') + 1;
    let courseId = parts[courseIdIndex];
    this.coursesFacade.dispatchGetSingleCourse(courseId);

     this.courseData$ = this.coursesFacade.course$
  }

  onClose() {
    this.router.navigate(['/course'])
  }
}
