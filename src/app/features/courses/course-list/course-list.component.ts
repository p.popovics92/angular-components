import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Author } from '@app/shared/interfaces/authors';
import { Course } from '@app/shared/interfaces/course';
import { CoursesFacade } from '@app/store/courses/courses.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {
  courseList$!: Observable<Course[]>;
  authorList: Array<Author> = [];
  authorMap: Map<string, string> = new Map<string, string>();
  @Output() editCourse: EventEmitter<Course> = new EventEmitter();
  @Output() deleteCourse: EventEmitter<Course> = new EventEmitter();

  constructor(private coursesFacade: CoursesFacade) {}

  ngOnInit(): void {
    this.onStart();
    this.coursesFacade.dispatchGetAllCourses();
  }

  onStart(){
    this.courseList$ = this.coursesFacade.allCourses$
  }

  applyFilter(searchTerm: string) {
    if (searchTerm) {
      this.coursesFacade.dispatchGetFilteredCourses(searchTerm);
    } 
    if (!searchTerm) {
      this.coursesFacade.dispatchGetAllCourses();
    }
  }
}
