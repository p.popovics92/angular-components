import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseInfoComponent } from '../course-info/course-info.component';
import { CoursesComponent } from './courses.component';
import { CourseListComponent } from './course-list/course-list.component';
import { SharedModule } from '@app/shared/shared.module';
import { CourseRoutingModule } from './course.routing.module';
import { CourseCardComponent, CourseFormComponent } from '@app/shared/components';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CourseInfoComponent,
    CoursesComponent,
    CourseListComponent,
    CourseCardComponent,
    CourseFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CourseRoutingModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    CourseInfoComponent,
    CoursesComponent,
    CourseListComponent
  ]
})
export class CoursesModule { }
