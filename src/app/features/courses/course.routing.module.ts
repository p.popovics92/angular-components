import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from './courses.component';
import { CourseFormComponent } from '@app/shared/components';
import { CourseInfoComponent } from '../course-info/course-info.component';
import { NgModule } from '@angular/core';
import { AdminGuard } from '@app/user/guards/admin.guard';
import { CourseListComponent } from './course-list/course-list.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
    children: [
      { path: '', pathMatch: 'full', component: CourseListComponent }, // Redirect to main course page if no path specified
      { path: 'add', component: CourseFormComponent, canActivate: [AdminGuard] },
      { path: ':id/edit', component: CourseFormComponent, canActivate: [AdminGuard] },
      { path: ':id', component: CourseInfoComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule { }
