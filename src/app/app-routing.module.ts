import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginFormComponent, RegistrationFormComponent } from './shared/components';
import { NgModule } from '@angular/core';
import { AuthorizedGuard } from './auth/guards/authorized.guard';
import { NotAuthorizedGuard } from './auth/guards/not-authorized.guard';

const routes: Routes = [
   {path: '', redirectTo: '/course', pathMatch: 'full'},
   {path: 'login', component:  LoginFormComponent, canActivate: [NotAuthorizedGuard] },
   {path: 'registration', component:  RegistrationFormComponent, canActivate: [NotAuthorizedGuard] },
   {path: 'course', 
            canLoad: [AuthorizedGuard],
            loadChildren: () => import('./features/courses/courses.module')
               .then(m => m.CoursesModule)
   },
];
@NgModule({
   imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
   exports: [RouterModule]
 })
 export class AppRoutingModule { }
