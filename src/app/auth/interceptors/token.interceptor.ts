import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionStorageService } from '../services/session-storage.service';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private sessionStorageService: SessionStorageService){}

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const idToken = this.sessionStorageService.getToken();
 
    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', idToken),
      });
 
      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}

