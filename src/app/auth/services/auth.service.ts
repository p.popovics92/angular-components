import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, tap } from 'rxjs';
import { SessionStorageService } from './session-storage.service';
import { User } from '@app/shared/interfaces/user';
import { UserLoginResponse } from '@app/shared/interfaces/user-response';
import { UserStoreService } from '@app/user/services/user-store.service';

@Injectable()
export class AuthService {
    private baseUrl: string = "http://localhost:4000";
    private isAuthorised$$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public isAuthorised$: Observable<boolean> = this.isAuthorised$$.asObservable();

    constructor(private http: HttpClient,
                private userStoreServices: UserStoreService,
                private sessionStorageService: SessionStorageService) {
        
        this.isAuthorised$$.next(!!this.sessionStorageService.getToken())
                }

    login(user: User) { 
        return this.http.post<UserLoginResponse>(`${this.baseUrl}/login`,
        {
            email: user.email,
            password: user.password,
        })
        .pipe(
            //catchError( this.errorhendler ), 
            tap( (respData: UserLoginResponse) => {   
                this.sessionStorageService.setToken(respData.result);
                this.isAuthorised = true;
                console.log(this.isAuthorised);
                console.log(respData);                  
            })

        );
    }

    logout() {
        return this.http.delete<User>(`${this.baseUrl}/logout`)
            .pipe(
                tap(() => {
                    this.sessionStorageService.deleteToken();
                    this.isAuthorised = false;
                    this.userStoreServices.name = '';
                    this.userStoreServices.isAdmin = false;
                })
            )
    }

    register(user: User) { 
        return this.http.post<User>(`${this.baseUrl}/register`,
        {
            name: user.name,
            email: user.email,
            password: user.password,
        })
        .pipe(
            //catchError( this.errorhendler ), 
            tap( respData => {   
                console.log(respData);                  
            })

        );
    }

    get isAuthorised() {
        return this.isAuthorised$$.getValue();
    }

    set isAuthorised(value: boolean) {
        this.isAuthorised$$.next(value);
    }

    getLoginUrl() {
        
    }

    
}
