import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Author, AuthorResponse } from '@app/shared/interfaces/authors';
import { Course, CourseDTO, CourseResponse } from '@app/shared/interfaces/course';
import { Observable, map } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CoursesService {
    private baseUrl: string = "http://localhost:4000";

    constructor(private http: HttpClient){}

    getAll(): Observable<CourseDTO[]> {
        return this.http.get<CourseResponse<CourseDTO[]>>(`${this.baseUrl}/courses/all`)
            .pipe(
                map(response => response.result)
            );
    }

    createCourse(course: CourseDTO) { 
        return this.http.post<CourseDTO>(`${this.baseUrl}/courses/add`, 
            {
                id: course.id,
                title: course.title,
                description: course.description,
                creationDate: course.creationDate,
                duration: course.duration,
                authors: course.authors
            }
        )
    }

    editCourse(id: string, course: CourseDTO) { 
        console.log(course)
        return this.http.put<CourseDTO>(`${this.baseUrl}/courses/${id}`, 
            {
                id: course.id,
                title: course.title,
                description: course.description,
                creationDate: course.creationDate,
                duration: course.duration,
                authors: course.authors
            }
        )
    }

    getCourse(id: string) {
        return this.http.get<CourseResponse<CourseDTO>>(`${this.baseUrl}/courses/${id}`)
            .pipe(
                map(response => response.result)
            )
    }

    deleteCourse(id: string | number) {
        return this.http.delete<Course>(`${this.baseUrl}/courses/${id}`)
    }

    filterCourses(title: string): Observable<Course[]> {
        const params = new HttpParams()
          .set('title', title)
        console.log(`http://localhost:4000/courses/filter?${params.toString()}`)
    
        return this.http.get<CourseResponse<Course[]>>(`http://localhost:4000/courses/filter?${params.toString()}`)
            .pipe(
                map(response =>  response.result)
            );
    }
    

    getAllAuthors() {
        return this.http.get<AuthorResponse<Author[]>>(`${this.baseUrl}/authors/all`)
            .pipe(
                map(response => response.result)
            )
    }

    mapAuthors(courseList: CourseDTO | CourseDTO[], authorList: Author[]) {
        const authorMap = new Map<string, Author>();
        authorList.forEach(author => {
            authorMap.set(author.id, author);
        });
        
        const courses = Array.isArray(courseList) ? courseList : [courseList];
    
        return courses.map(course => {
            const mappedAuthors = course.authors ? course.authors.map(authorId => authorMap.get(authorId)!) : [];
            return {...course, authors: mappedAuthors};
        });
    }

    createAuthor(author: Author) {
        return this.http.post<Author>(`${this.baseUrl}/authors/add`,
        {
            id: author.id, 
            name: author.name
        }
        )
    }

    getAuthorById(id: string) {
        return this.http.get<AuthorResponse<Author>>(`${this.baseUrl}/author/${id}`)
            .pipe(
                map(response => response.result)
            )
    }
}
