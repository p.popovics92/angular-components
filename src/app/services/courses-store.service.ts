// import { Injectable } from '@angular/core';
// import { Course, CourseResponse } from '@app/shared/interfaces/course';
// import { BehaviorSubject, Observable } from 'rxjs';
// import { CoursesService } from './courses.service';
// import { Author } from '@app/shared/interfaces/authors';

// @Injectable({
//     providedIn: 'root'
// })
// export class CoursesStoreService {
//     private isLoading$$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
//     private courses$$: BehaviorSubject<Course[]> = new BehaviorSubject<Course[]>([]);
//     private authors$$: BehaviorSubject<Author[]> = new BehaviorSubject<Author[]>([]);
//     private isEdit$$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

//     isLoading$: Observable<boolean> = this.isLoading$$.asObservable();
//     courses$: Observable<Course[]> = this.courses$$.asObservable();
//     public isEdit$: Observable<boolean> = this.isEdit$$.asObservable();
//     public authors$: Observable<Author[]> = this.authors$$.asObservable();

//     constructor(private coursesService: CoursesService){}

//     get isEdit() {
//       return this.isEdit$$.getValue()
//     }

//     set isEdit(value: boolean) {
//       this.isEdit$$.next(value)
//     }

//     getAll(){
//         this.isLoading$$.next(true);
//         this.coursesService.getAll().subscribe(
//         courses => {
//             this.courses$$.next(courses);
//             this.isLoading$$.next(false);
//         },
//         error => {
//             console.error('Error fetching courses:', error);
//             this.isLoading$$.next(false);
//         }
//       );
//     }

//     createCourse(course: Course) {
//         this.isLoading$$.next(true);
//         this.coursesService.createCourse(course).subscribe(
//           () => {
//             this.getAll(); 
//           },
//           error => {
//             console.error('Error creating course:', error);
//             this.isLoading$$.next(false);
//           }
//         );
//       }

//     getAllAuthors() {
//         this.isLoading$$.next(true);
//         this.coursesService.getAllAuthors().subscribe(
//           (authors: Author[]) => {
//             this.authors$$.next(authors);
//             this.isLoading$$.next(false);
//           },
//           error => {
//             console.error('Error loading authors:', error);
//             this.isLoading$$.next(false);
//           }
//         );
//     }

//     editCourse(id: string, course: Course) {
//         this.isLoading$$.next(true);
//         this.coursesService.editCourse(id, course).subscribe(
//           () => {
//             this.getAll(); 
//           },
//           error => {
//             console.error('Error editing course:', error);
//             this.isLoading$$.next(false);
//           }
//         );
//       }
    
//     getCourse(id: string) {
//     this.isLoading$$.next(true);
//     return this.coursesService.getCourse(id);
//     }

//     deleteCourse(id: string) {
//       this.isLoading$$.next(true);
//       this.coursesService.deleteCourse(id).subscribe(
//         () => {
//           this.getAll();
//         },
//         error => {
//           console.error('Error deleting course:', error);
//           this.isLoading$$.next(false);
//         }
//       );
//     }
    
//     filterCourses(value: string) {
//       this.isLoading$$.next(true);
//       this.coursesService.filterCourses(value).subscribe((filterdCourses: Course[]) => {
//         console.log(this.courses$)
//         this.courses$$.next(filterdCourses);
//         this.isLoading$$.next(false);
//       },
//       error => {
//         console.error('Error fetching filtered courses:', error);
//         this.isLoading$$.next(false);
//       }
//       );
//     }
    
//     createAuthor(name: string) {
//     this.isLoading$$.next(true);
//     this.coursesService.createAuthor(name).subscribe(
//         () => {
//         this.getAllAuthors();
//         },
//         error => {
//         console.error('Error creating author:', error);
//         this.isLoading$$.next(false);
//         }
//     );
//     }
    
    
//     getAuthorById(id: string) {
//     this.isLoading$$.next(true);
//     return this.coursesService.getAuthorById(id);
//     }
// }
