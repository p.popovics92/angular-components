import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AuthService } from './auth/services/auth.service';
import { Router } from '@angular/router';
import { UserStoreService } from './user/services/user-store.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isAdmin: boolean = false;
  name$: Observable<string> = new Observable<string>();

  constructor(private authService: AuthService, private router: Router, private userStorageService: UserStoreService, private cdr: ChangeDetectorRef){}

  ngOnInit(): void {
    this.userStorageService.getUser().subscribe(() => {
      this.isAdmin = this.userStorageService.isAdmin;
      this.name$ = this.userStorageService.name$;
    });
  }

  onLogout(){
    this.authService.logout()
      .subscribe(() => {

        this.router.navigate(['/login'])
      });
  }
}
