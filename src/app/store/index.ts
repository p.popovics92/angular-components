import { CoursesState, coursesReducer } from "./courses/courses.reducer";
import { CoursesEffects } from "./courses/courses.effects";
import { ActionReducerMap } from "@ngrx/store";

export interface AppState {
    courses: CoursesState
}

export const reducers: ActionReducerMap<AppState> = {
    courses: coursesReducer
};

export const effects = [
    CoursesEffects
];