import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { Course, CourseDTO } from '@app/shared/interfaces/course';
import {
  requestAllCourses,
  requestSingleCourse,
  requestFilteredCourses,
  requestEditCourse,
  requestCreateCourse,
  requestDeleteCourse
} from './courses.actions';
import {
  isAllCoursesLoadingSelector,
  isSingleCourseLoadingSelector,
  isSearchingStateSelector,
  getCourses,
  getAllCourses,
  getCourse,
  getErrorMessage
} from './courses.selectors';


@Injectable({
  providedIn: 'root'
})
export class CoursesFacade {
  constructor(private store: Store) {}

  isAllCoursesLoading$: Observable<boolean> = this.store.pipe(select(isAllCoursesLoadingSelector));
  isSingleCourseLoading$: Observable<boolean> = this.store.pipe(select(isSingleCourseLoadingSelector));
  isSearchingState$: Observable<boolean> = this.store.pipe(select(isSearchingStateSelector));
  courses$: Observable<Course[]> = this.store.pipe(select(getCourses));
  allCourses$: Observable<Course[]> = this.store.pipe(select(getAllCourses));
  course$: Observable<Course | null> = this.store.pipe(select(getCourse));
  errorMessage$: Observable<string | null> = this.store.pipe(select(getErrorMessage));

  private isEdit$$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
  public isEdit$: Observable<boolean> = this.isEdit$$.asObservable();
  get isEdit() {
    return this.isEdit$$.getValue()
  }
  set isEdit(value: boolean) {
    this.isEdit$$.next(value)
  }

  dispatchGetAllCourses(): void {
    this.store.dispatch(requestAllCourses());
  }

  dispatchGetSingleCourse(id: string): void {
    this.store.dispatch(requestSingleCourse({ id }));
  }

  dispatchGetFilteredCourses(title: string): void {
    this.store.dispatch(requestFilteredCourses({ title }));
  }

  dispatchEditCourse( id: string, course: Course): void {
    this.store.dispatch(requestEditCourse({ id,  course }));
  }

  dispatchCreateCourse(course: Course): void {
    this.store.dispatch(requestCreateCourse({ course }));
  }

  dispatchDeleteCourse(id: string): void {
    this.store.dispatch(requestDeleteCourse({ id }));
  }
}
