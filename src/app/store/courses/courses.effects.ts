import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { combineLatest, concatMap, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs';
import {
  requestAllCourses,
  requestAllCoursesSuccess,
  requestAllCoursesFail,
  requestSingleCourse,
  requestSingleCourseSuccess,
  requestSingleCourseFail,
  requestFilteredCourses,
  requestFilteredCoursesSuccess,
  requestFilteredCoursesFail,
  requestEditCourse,
  requestEditCourseSuccess,
  requestEditCourseFail,
  requestCreateCourse,
  requestCreateCourseSuccess,
  requestCreateCourseFail,
  requestDeleteCourse,
  requestDeleteCourseSuccess
} from './courses.actions';
import { Router } from '@angular/router';
import { CoursesService } from '@app/services/courses.service';
import { Course } from '@app/shared/interfaces/course';
import { Action } from '@ngrx/store';

@Injectable()
export class CoursesEffects {
  constructor(private actions$: Actions, private coursesService: CoursesService, private router: Router) { }

  getAll$ = createEffect(() => this.actions$.pipe(
    ofType(requestAllCourses),
    switchMap(() =>
      combineLatest([this.coursesService.getAll(), this.coursesService.getAllAuthors()]).pipe(
        map(([courses, authors]) => {
          const courseAuthors = this.coursesService.mapAuthors(courses, authors);
          return requestAllCoursesSuccess({ courses: courseAuthors })
        }),
        catchError(error => of(requestAllCoursesFail({ error })),
        )))));

  filteredCourses$ = createEffect(() => this.actions$.pipe(
    ofType(requestFilteredCourses),
    switchMap(({ title }) =>
      this.coursesService.filterCourses(title).pipe(
        map(courses => requestFilteredCoursesSuccess({ courses })),
        catchError(error => of(requestFilteredCoursesFail({ error })))
      )
    )
  )//, { dispatch: true } // rákérdezni
  );

  getSpecificCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestSingleCourse),
    switchMap(({ id }) => combineLatest([this.coursesService.getCourse(id), this.coursesService.getAllAuthors()]).pipe(
      map(([course, authors]) => {
        const courseAuthors = this.coursesService.mapAuthors(course, authors);
        return requestSingleCourseSuccess({ course: courseAuthors[0] })
      }),
      catchError(error => of(requestAllCoursesFail({ error })),
      )))
  ));

  deleteCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestDeleteCourse),
    switchMap(({ id }) => this.coursesService.deleteCourse(id).pipe(
      concatMap(() => {
        const actions: Action[] = [
          requestDeleteCourseSuccess(),
          requestAllCourses()
        ];
        return actions;
      }),
      catchError(error => of(requestSingleCourseFail({ error })))
    ))
  ));

  editCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestEditCourse),
    switchMap(({ course }) => {
      const DTOCourse = {...course, authors: course.authors.map(author => author.id)}
      return this.coursesService.editCourse(course.id, DTOCourse).pipe(
        map(() => requestEditCourseSuccess()),
        catchError(error => of(requestEditCourseFail({ error })))
    )})
  ));

  createCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestCreateCourse),
    switchMap(({ course }) => {
      const DTOCourse = {...course, authors: course.authors.map(author => author.id)}
      return this.coursesService.createCourse(DTOCourse).pipe(
        concatMap(() =>{
          const actions = [
            requestCreateCourseSuccess(),
            requestAllCourses()
          ]
          return actions
          }),
        catchError(error => of(requestCreateCourseFail({ error })))
    )})
  ));

  redirectToTheCoursesPage$ = createEffect(() => this.actions$.pipe(
    ofType(requestCreateCourseSuccess, requestEditCourseSuccess, requestSingleCourseFail),
    tap(() => {
      this.router.navigate(['/course']);
    })
  ), { dispatch: false });

}
