import { Course } from '@app/shared/interfaces/course';
import { Action, createReducer, on } from '@ngrx/store';
import { requestAllCourses, 
         requestAllCoursesFail, 
         requestAllCoursesSuccess, 
         requestCreateCourse, 
         requestCreateCourseFail, 
         requestCreateCourseSuccess, 
         requestDeleteCourse, 
         requestDeleteCourseFail, 
         requestDeleteCourseSuccess, 
         requestEditCourse, 
         requestEditCourseFail, 
         requestEditCourseSuccess, 
         requestFilteredCourses, 
         requestFilteredCoursesFail, 
         requestFilteredCoursesSuccess, 
         requestSingleCourse, 
         requestSingleCourseFail, 
         requestSingleCourseSuccess 
        } from './courses.actions';
import { state } from '@angular/animations';

export const coursesFeatureKey = 'courses';

export interface CoursesState {
    allCourses: Course[];
    course: Course | null;
    isAllCoursesLoading: boolean;
    isSingleCourseLoading: boolean;
    isSearchState: boolean;
    errorMessage: string | null;
}

export const initialState: CoursesState = {
    allCourses: [],
    course: null,
    isAllCoursesLoading: false,
    isSingleCourseLoading: false,
    isSearchState: false,
    errorMessage: null
};

export const coursesReducer = createReducer(
    initialState,
    on(requestAllCourses, state => ({
        ...state,
        isAllCoursesLoading: true,
        errorMessage: null 
      })),
    on(requestAllCoursesSuccess, (state, { courses }) => ({
        ...state,
        allCourses: courses,
        isAllCoursesLoading: false
    })),
    on(requestAllCoursesFail, (state, { error }) => ({
        ...state,
        isAllCoursesLoading: false,
        errorMessage: error
    })),
    on(requestSingleCourse, state => ({
        ...state,
        isSingleCourseLoading: true,
        errorMessage: null
    })),
    on(requestSingleCourseSuccess, (state, { course }) => ({
        ...state,
        course,
        isSingleCourseLoading: false
    })),
    on(requestSingleCourseFail, (state, { error }) => ({
        ...state,
        isSingleCourseLoading: false,
        errorMessage: error 
    })),
    on(requestFilteredCourses, state => ({
        ...state,
        isSearchState: true,
        errorMessage: null
    })),
    on(requestFilteredCoursesSuccess, (state, { courses }) => ({
        ...state,
        allCourses: courses,
        isSearchState: false
    })),
    on(requestFilteredCoursesFail, (state, { error }) => ({
        ...state,
        isSearchState: false,
        errorMessage: error 
    })),
    on(requestEditCourse, state => ({
        ...state,
        isSingleCourseLoading: true,
        errorMessage: null 
    })),
    on(requestEditCourseSuccess, state => ({
        ...state,
        isSingleCourseLoading: false,
    })),
    on(requestEditCourseFail, (state, { error }) => ({
        ...state,
        isSingleCourseLoading: false,
        errorMessage: error 
    })),
    on(requestCreateCourse, state => ({
        ...state, 
        errorMessage: null
    })),
    on(requestCreateCourseSuccess, state => ({
        ...state, 
    })),
    on(requestCreateCourseFail, (state, { error }) => ({
        ...state,
        errorMessage: error 
    })),
    on(requestDeleteCourse, state => ({
        ...state,
        errorMessage: null
    })),
    on(requestDeleteCourseSuccess, state => ({
        ...state, 
    })),
    on(requestDeleteCourseFail, (state, { error }) => ({
        ...state,
        errorMessage: error 
    }))
)

export const reducer = (state: CoursesState, action: Action): CoursesState => coursesReducer(state, action);
