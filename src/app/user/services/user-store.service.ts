import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { UserService } from './user.service';
import { UserResponse } from '@app/shared/interfaces/user.response';

@Injectable({
    providedIn: 'root'
})
export class UserStoreService {
    private isAdmin$$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    private name$$: BehaviorSubject<string> = new BehaviorSubject('');
    public isAdmin$: Observable<boolean> = this.isAdmin$$.asObservable();
    public name$: Observable<string> = this.name$$.asObservable();

    constructor(private userService: UserService) {}

    getUser(): Observable<any> {
        return this.userService.getUser()
        .pipe(
            tap((data: UserResponse) => {
                this.name$$.next(data.result.name);
              if(data.result.role === "admin") { 
                this.isAdmin$$.next(true);
              } else {
                this.isAdmin$$.next(false);
              }
            }))
      }

    get isAdmin() {
        return this.isAdmin$$.getValue();
    }

    set isAdmin(value: boolean) {
        this.isAdmin$$.next(value);
    }

    get name() {
        return this.name$$.getValue();
    }

    set name(value: string) {
        this.name$$.next(value);
    }
}
