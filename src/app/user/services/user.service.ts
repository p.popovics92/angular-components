import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserResponse } from '@app/shared/interfaces/user.response';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private baseUrl: string = "http://localhost:4000";

    constructor(private http: HttpClient) {}

    getUser() {
        return this.http.get<UserResponse>(`${this.baseUrl}/users/me`)
    }
}
