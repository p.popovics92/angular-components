import { User } from "./user";

export interface UserLoginResponse {
    result: string;
    successful: boolean;
    user: User
}