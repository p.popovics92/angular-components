export interface UserResponse {
    succesful: boolean
    result: {
        email: string,
        id: string,
        name: string,
        password: string,
        role: string
    }
}