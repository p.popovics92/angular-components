import { v4 as uuidv4 } from 'uuid';
import { Author } from './authors';

export interface CourseDTO {
    id: string;
    title: string;
    description: string;
    creationDate: string;
    duration: number;
    authors: Array<string>;
}

export class Course {
    id: string;
    title: string;
    description: string;
    creationDate: string;
    duration: number;
    authors: Array<Author>;

    constructor(title: string, description: string, duration: number, authors: Array<Author>) {
       this.id = uuidv4();
       this.title = title;
       this.description = description;
       this.creationDate = new Date().toISOString();
       this.duration = duration;
       this.authors = authors;
    }
}


export interface CourseResponse<R> {
    result: R;
    success: boolean;
}
