import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CoursesFacade } from '@app/store/courses/courses.facade';
import { UserStoreService } from '@app/user/services/user-store.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  searchTerm: string = ''
  isAdmin: boolean = false;

  constructor(private userStoreService: UserStoreService, private router: Router, private coursesFacade: CoursesFacade) {}

  ngOnInit() {
    this.userStoreService.isAdmin$.subscribe((adminRole) => {this.isAdmin = adminRole})
  }

  searchCourses() {
    this.search.emit(this.searchTerm);
  }

  onAdd() {
    this.coursesFacade.isEdit = false;
    this.router.navigate(['/course/add']);
  }
}

