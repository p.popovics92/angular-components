import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailValidatorDirective } from '../../directives/email.directive';
import { AuthService } from '@app/auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
})
export class RegistrationFormComponent implements OnInit{
  registrationForm: FormGroup = <FormGroup>{};
  // Use the names `name`, `email`, `password` for the form controls.

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    let name: string = '';
    let email: string = '';
    let password: string = '';

    this.registrationForm = new FormGroup({
      'name': new FormControl(name, [
        Validators.required,
        Validators.minLength(6)
      ]),
      'email': new FormControl(email, [
        Validators.required,
        EmailValidatorDirective.validate
      ]),
      'password': new FormControl(password, [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*'"-]).{8,}$/)
      ])
    })
  }

  onSubmit() {
    let user = {...this.registrationForm.value};
    this.authService.register(user)
      .subscribe( () => {this.router.navigate(['/login'])});
    console.log(this.registrationForm.value)
    console.log(this.registrationForm.valid)
  }
}
