import { Component, Input, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Author } from '@app/shared/interfaces/authors';
import { Course } from '@app/shared/interfaces/course';
import { CoursesFacade } from '@app/store/courses/courses.facade';
import { UserStoreService } from '@app/user/services/user-store.service';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent implements OnInit {
  @Input('editable') editableInput: boolean = false;
  @Input('course') courseData: Course = <Course>{};
  @Output() editCourse: string = '';
  @Output() deleteCourse: string = '';
  
  id: string = '';
  title: string = '';
  description: string = ''
  creationDate: string = '';
  duration: number = 0;
  authors: Author[] = [];
  editable: boolean = false;

  constructor(private coursesFacade: CoursesFacade,
              private userStoreService: UserStoreService,
              private router: Router) {
              this.userStoreService.isAdmin$.subscribe((isAdmin) => {
                this.editable = isAdmin;
              })
  }

  ngOnInit(): void {
    this.id = this.courseData.id;
    this.title = this.courseData.title;
    this.description = this.courseData.description;
    this.creationDate = this.courseData.creationDate;
    this.duration = this.courseData.duration;
    this.authors = this.courseData.authors;

    this.userStoreService.getUser().subscribe(() => {
   });
  }

  clickOnShow(id: string) {
    this.router.navigate([`/course/${id}`])
  }

  onEditCourse(courseId: string) {
    this.coursesFacade.isEdit = true;
    this.router.navigate([`/course/${courseId}/edit`])
  }

  onDeleteCourse(courseName: string) {
    this.coursesFacade.dispatchDeleteCourse(courseName);
  }
}
