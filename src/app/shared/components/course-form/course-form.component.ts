import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder, FormControl, FormGroup, Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { CoursesService } from '@app/services/courses.service';
import { Author } from '@app/shared/interfaces/authors';
import { Course } from '@app/shared/interfaces/course';
import { CoursesFacade } from '@app/store/courses/courses.facade';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { Observable, Subscription, map } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss'],
})
export class CourseFormComponent implements OnInit, OnDestroy  {
  isEdit: boolean = false;
  courseForm: FormGroup = <FormGroup>{};
  templateAuthors: Array<Author> = [];
  authorFormControl: FormControl = new FormControl('');
  assignedAuthorsToCourse: Array<Author> = [];
  newAuthorError: string = '';
  editableCourse: Course = <Course>{};

  private getAllAuthorsSubscription!: Subscription;
  private isEditSubscription!: Subscription;
  private courseSubscription!: Subscription;
  private errorMessageSubscription!: Subscription;

  constructor(public fb: FormBuilder, public library: FaIconLibrary, 
              private cdr: ChangeDetectorRef, 
              private coursesService: CoursesService,
              private coursesFacade: CoursesFacade,
              private router: Router) {
    library.addIconPacks(fas);
  }

  ngOnInit(): void {
    this.getAllAuthorsSubscription = this.coursesService.getAllAuthors()
      .subscribe((data) => {
        this.templateAuthors = data;
      })
    this.initForm();

    this.isEditSubscription = this.coursesFacade.isEdit$.subscribe((isEdit) => {
      if(isEdit) {
        this.isEdit = true;
        const url = this.router.url;
        const parts = url.split('/');
        const courseIdIndex = parts.indexOf('course') + 1;
        const courseId = parts[courseIdIndex];
    
        if (courseId && this.isEdit) { 
          this.coursesFacade.dispatchGetSingleCourse(courseId);
          this.courseSubscription = this.coursesFacade.course$.subscribe(data => {
            if (data) { 
              this.editableCourse = data;
              this.assignedAuthorsToCourse = [...data.authors];
              this.templateAuthors = this.templateAuthors.filter( (templateAuthor) => !data.authors.map(author => author.id).includes(templateAuthor.id))
              this.initializeForm(); 
              this.coursesFacade.isEdit = false;
            } 
          });
          this.errorMessageSubscription = this.coursesFacade.errorMessage$.subscribe(data => console.log(data));
        } else {
          this.isEdit = false;
          this.editableCourse = <Course>{};
          this.assignedAuthorsToCourse = [];
          this.courseForm.reset();
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.getAllAuthorsSubscription) {
      this.getAllAuthorsSubscription.unsubscribe();
    }
    if (this.isEditSubscription) {
      this.isEditSubscription.unsubscribe();
    }
    if (this.courseSubscription) {
      this.courseSubscription.unsubscribe();
    }
    if (this.errorMessageSubscription) {
      this.errorMessageSubscription.unsubscribe();
    }
  }

  initForm() {
      this.courseForm = this.fb.group({
        title: ['', [Validators.required, Validators.minLength(2)]],
        description: ['', [Validators.required, Validators.minLength(2)]],
        authors: this.fb.array([],  Validators.required),
        duration: [0,  Validators.required],
        id: [uuidv4()],
        creationDate: [this.getCurrentDateFormatted()]
      });
  }

  initializeForm() {
   
    
    this.courseForm = this.fb.group({
      title: [this.editableCourse.title, [Validators.required, Validators.minLength(2)]],
      description: [this.editableCourse.description, [Validators.required, Validators.minLength(2)]],
      authors: this.fb.array(this.editableCourse.authors, Validators.required),
      duration: [this.editableCourse.duration, Validators.required],
      id: [this.editableCourse.id],
      creationDate: [this.editableCourse.creationDate]
    });
  }

  get authors(): FormArray {
    return this.courseForm.get('authors') as FormArray;
  }

  get courseDuration(): FormControl {
    return this.courseForm.get('courseDuration') as FormControl;
  }

  getCurrentDateFormatted(): string {
    const currentDate = new Date();
    const day = currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate();
    const month = (currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1);
    const year = currentDate.getFullYear();
    return `${day}/${month}/${year}`;
  }

  onAddNewAuthor(authorName: string) {
    this.newAuthorError = '';
    if (!authorName || authorName.trim().length < 2) {
      this.newAuthorError = 'Author name must be at least 2 characters long!';
      return;
    }
  
    if (this.templateAuthors.some(author => author.name === authorName.trim())) {
      this.newAuthorError = 'Author already exists!';
      return;
    }

    const author: Author = new Author(authorName.trim());
    this.coursesService.createAuthor(author).subscribe(()=> {console.log(author)});
    this.templateAuthors.push(author);
    this.authorFormControl.setValue('');
    this.cdr.detectChanges();
  }

  onAddAuthor(author: Author) {
    const index = this.templateAuthors.findIndex(a => a === author);

    if (index !== -1) {
      const control = this.fb.control(author);
      this.assignedAuthorsToCourse.push(author);

      // Use the FormArray's push method to add a new control
      (this.authors as FormArray).push(control);
      
      this.templateAuthors.splice(index, 1);
      this.cdr.detectChanges();
    }
  }
  

  onDeleteAuthor(author: Author) {
    const index = this.templateAuthors.findIndex(a => a === author);
    if (index !== -1) {
      this.templateAuthors.splice(index, 1);
    }
  }

  onRemoveAuthor(index: number) {
    const removedAuthor = this.authors.at(index).value;
    this.authors.removeAt(index);
    this.templateAuthors.push(removedAuthor);
  }

  getAuthorIdByName(authorArray: Array<string>): Observable<Array<string>> {
    return this.coursesService.getAllAuthors()
      .pipe(
        map((authors: Author[]) => {
          return authorArray.map(authorName => {
            const author = authors.find(a => a.name === authorName);
            return author ? author.id : null;
          });
        }),
        map(authorIds => authorIds.filter(id => id !== null) as string[])
      );
  }
  
  onSubmit() {
    let newCourse = this.courseForm.value;
    if (this.courseForm.valid) {  
      if (this.isEdit) {

        this.isEdit = false;
        this.coursesFacade.dispatchEditCourse(newCourse.id, newCourse);
      } else {
        this.isEdit = false;
        this.coursesFacade.dispatchCreateCourse(newCourse);
      } 
      this.router.navigate(['/course']);
    }
  }
  
}
