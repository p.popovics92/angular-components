import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailValidatorDirective } from '../../directives/email.directive';
import { AuthService } from '@app/auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit{
  //@ViewChild("loginForm") public loginForm!: NgForm;
  loginForm: FormGroup = <FormGroup>{};

  constructor(private authService: AuthService,
              private router: Router) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    let email: string = '';
    let password: string = '';

    this.loginForm = new FormGroup({
      'email': new FormControl(email, [
        Validators.required,
        EmailValidatorDirective.validate,
        
      ]),
      'password': new FormControl(password, [
        Validators.required
      ]),
    })
  }

  onSubmit(){
    let user: any = {...this.loginForm.value};
    this.authService.login(user)
      .subscribe(() => {this.router.navigate(['/course'])});
  }
}
