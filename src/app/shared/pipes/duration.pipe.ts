import { Pipe } from "@angular/core";

@Pipe({
    name: 'duration'
})
export class DurationPipe {
    transform(value: number): string {
        if (!value || isNaN(value) || value < 0) {
            return 'Invalid duration';
        }

        const hours = Math.floor(value / 60);
        const minutes = value % 60;

        const formattedHours = this.padWithZero(hours);
        const formattedMinutes = this.padWithZero(minutes);

        return `${formattedHours}:${formattedMinutes} hours`;
    }

    private padWithZero(value: number): string {
        return value < 10 ? `0${value}` : `${value}`;
    }
}
