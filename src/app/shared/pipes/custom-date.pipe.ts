import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {
    transform(value: any): string {
        if (value === null || value === undefined) {
          return '';
        }
    
        const datePipe = new DatePipe('en-US');
        return datePipe.transform(value, 'dd.MM.yyyy') || '';
      }
  }
